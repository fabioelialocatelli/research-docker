#!/bin/sh

mkdir -p /mnt/docker/dynatrace/
cd /mnt/docker/dynatrace/

mkdir -p hadar
docker run -t -d -v /mnt/docker/dynatrace/hadar:/opt/dynatrace/appmon/server/conf --name="hadar" --hostname="Hadar" --network="galaxy" --mac-address="02:42:ac:11:00:11" --ip="172.41.0.2" -p 2021:2021 -p 8023:8023 -p 6699:6699 -p 8033:8033 -p 9998:9998 appmon:7.1
docker exec hadar bash /etc/init.d/dynaTraceServer start

mkdir -p graffias
docker run -t -d -v /mnt/docker/dynatrace/graffias:/opt/dynatrace/appmon/server/conf --name="graffias" --hostname="Graffias" --network="galaxy" --mac-address="02:42:ac:11:00:12" --ip="172.41.0.3" -p 2022:2021 -p 8024:8023 -p 6700:6699 -p 8034:8033 -p 9999:9998 appmon:7.1
docker exec graffias bash /etc/init.d/dynaTraceServer start

mkdir -p achernar
docker run -t -d -v /mnt/docker/dynatrace/achernar:/opt/dynatrace/appmon/server/conf --name="achernar" --hostname="Achernar" --network="galaxy" --mac-address="02:42:ac:11:00:13" --ip="172.41.0.4" -p 2023:2021 -p 8025:8023 -p 6701:6699 -p 8035:8033 -p 10000:9998 appmon:7.1
docker exec achernar bash /etc/init.d/dynaTraceServer start

mkdir -p acamar
docker run -t -d -v /mnt/docker/dynatrace/acamar:/opt/dynatrace/appmon/server/conf --name="acamar" --hostname="Acamar" --network="galaxy" --mac-address="02:42:ac:11:00:14" --ip="172.41.0.5" -p 2024:2021 -p 8026:8023 -p 6702:6699 -p 8036:8033 -p 10001:9998 appmon:7.1
docker exec acamar bash /etc/init.d/dynaTraceServer start

mkdir -p aludra
docker run -t -d -v /mnt/docker/dynatrace/aludra:/opt/dynatrace/appmon/server/conf --name="aludra" --hostname="Aludra" --network="galaxy" --mac-address="02:42:ac:11:00:15" --ip="172.41.0.6" -p 2025:2021 -p 8027:8023 -p 6703:6699 -p 8037:8033 -p 10002:9998 appmon:7.1
docker exec aludra bash /etc/init.d/dynaTraceServer start

mkdir -p wezen
docker run -t -d -v /mnt/docker/dynatrace/wezen:/opt/dynatrace/appmon/server/conf --name="wezen" --hostname="Wezen" --network="galaxy" --mac-address="02:42:ac:11:00:16" --ip="172.41.0.7" -p 2026:2021 -p 8028:8023 -p 6704:6699 -p 8038:8033 -p 10003:9998 appmon:7.1
docker exec wezen bash /etc/init.d/dynaTraceServer start

docker network inspect galaxy

