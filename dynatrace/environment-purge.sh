#!/bin/sh

docker stop hadar
docker stop graffias
docker stop achernar
docker stop acamar
docker stop aludra
docker stop wezen

docker container prune -f
