#!/bin/sh

cd /mnt/docker/dynatrace/

truncate -s 0 acamar/repository.config.xml
truncate -s 0 achernar/repository.config.xml
truncate -s 0 aludra/repository.config.xml
truncate -s 0 graffias/repository.config.xml
truncate -s 0 hadar/repository.config.xml
truncate -s 0 wezen/repository.config.xml

cd /root/Documents/Git/research-docker/dynatrace/config/

cat server_acamar.xml > /mnt/docker/dynatrace/acamar/repository.config.xml
cat server_achernar.xml > /mnt/docker/dynatrace/achernar/repository.config.xml
cat server_aludra.xml > /mnt/docker/dynatrace/aludra/repository.config.xml
cat server_graffias.xml > /mnt/docker/dynatrace/graffias/repository.config.xml
cat server_hadar.xml > /mnt/docker/dynatrace/hadar/repository.config.xml
cat server_wezen.xml > /mnt/docker/dynatrace/wezen/repository.config.xml

