#!/bin/sh

docker stop tureis
docker stop asmidiske
docker stop aspidiske
docker stop suhail
docker stop lesath
docker stop shaula

docker container prune -f
