#!/bin/sh

docker pull mariadb:10

mkdir -p /mnt/docker/mysql/
cd /mnt/docker/mysql/

mkdir -p naos
docker run -e MYSQL_ROOT_PASSWORD=scopulinus43# -v /mnt/docker/mysql/tureis:/var/lib/mysql --name="naos" --hostname="Naos" --network="galaxy" --mac-address="02:42:ac:11:00:17" --ip="172.41.0.8" -d -p 3306:3306 mariadb:10 --default-authentication-plugin=mysql_native_password

mkdir -p asmidiske
docker run -e MYSQL_ROOT_PASSWORD=scopulinus43# -v /mnt/docker/mysql/asmidiske:/var/lib/mysql --name="asmidiske" --hostname="Asmidiske" --network="galaxy" --mac-address="02:42:ac:11:00:18" --ip="172.41.0.9" -d -p 3307:3306 mariadb:10 --default-authentication-plugin=mysql_native_password

mkdir -p aspidiske
docker run -e MYSQL_ROOT_PASSWORD=scopulinus43# -v /mnt/docker/mysql/aspidiske:/var/lib/mysql --name="aspidiske" --hostname="Aspidiske" --network="galaxy" --mac-address="02:42:ac:11:00:19" --ip="172.41.0.10" -d -p 3308:3306 mariadb:10 --default-authentication-plugin=mysql_native_password

mkdir -p suhail
docker run -e MYSQL_ROOT_PASSWORD=scopulinus43# -v /mnt/docker/mysql/suhail:/var/lib/mysql --name="suhail" --hostname="Suhail" --network="galaxy" --mac-address="02:42:ac:11:00:20" --ip="172.41.0.11" -d -p 3309:3306 mariadb:10 --default-authentication-plugin=mysql_native_password

mkdir -p lesath
docker run -e MYSQL_ROOT_PASSWORD=scopulinus43# -v /mnt/docker/mysql/lesath:/var/lib/mysql --name="lesath" --hostname="Lesath" --network="galaxy" --mac-address="02:42:ac:11:00:21" --ip="172.41.0.12" -d -p 3310:3306 mariadb:10 --default-authentication-plugin=mysql_native_password

mkdir -p shaula
docker run -e MYSQL_ROOT_PASSWORD=scopulinus43# -v /mnt/docker/mysql/shaula:/var/lib/mysql --name="shaula" --hostname="Shaula" --network="galaxy" --mac-address="02:42:ac:11:00:22" --ip="172.41.0.13" -d -p=3311:3306 mariadb:10 --default-authentication-plugin=mysql_native_password

docker network inspect galaxy

